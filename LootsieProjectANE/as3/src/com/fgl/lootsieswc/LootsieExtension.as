package com.fgl.lootsieswc
{
	import flash.external.ExtensionContext; 
	
	public class LootsieExtension 
	{ 
		private const extensionID:String = "com.fgl.LootsieExtension"; 
		private var extensionContext:ExtensionContext; 
		
		public function LootsieExtension() 
		{ 
			extensionContext = ExtensionContext.createExtensionContext( extensionID, null ); 
		} 
		
		// Initialize Lootsie with a public key
		public function init( key:String ):void 
		{ 			
			extensionContext.call( "initFunctionKey", key ); 			 
		}
		
		// Award the specified achievement
		public function awardAchievement( achievementId:String, positionId:String = ""):void 
		{ 			
			extensionContext.call( "showAchievementFunctionKey", achievementId, positionId ); 			 
		}
		
		// Show the web view
		public function showWebView():void 
		{ 			
			extensionContext.call( "showWebViewFunctionKey"); 			 
		}
	} 	
}