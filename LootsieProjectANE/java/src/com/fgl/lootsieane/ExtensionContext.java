package com.fgl.lootsieane; 

import java.util.HashMap;
import java.util.Map;

import android.util.Log;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
 
public class ExtensionContext extends FREContext { 
	private static final String CTX_NAME = "ExtensionContext"; 
	private String tag; 
	 
	public ExtensionContext( String extensionName ) { 
		tag = extensionName + "." + CTX_NAME; 
		Log.d(tag, "Creating context"); 
	} 
	 
	@Override 
	public void dispose() { 
		Log.d(tag, "Dispose context"); 
	} 
	 
	@Override 
	public Map<String, FREFunction> getFunctions() { 
		Log.d(tag, "Creating function Map"); 
		Map<String, FREFunction> functionMap = new HashMap<String, FREFunction>(); 
	 
		functionMap.put( InitFunction.KEY, new InitFunction() );
		functionMap.put( ShowWebViewFunction.KEY, new ShowWebViewFunction() );
		functionMap.put( ShowAchievementFunction.KEY, new ShowAchievementFunction() ); 
		return functionMap; 
	} 
	 
	public String getIdentifier() { 
		return tag; 
	} 
}
