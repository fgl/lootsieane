package com.fgl.lootsieane;

import com.adobe.fre.FREContext; 
import com.adobe.fre.FREFunction; 
import com.adobe.fre.FREObject; 
import com.lootsie.Lootsie;
 
import android.util.Log; 
 
public class ShowWebViewFunction implements FREFunction { 
	public static final String KEY = "showWebViewFunctionKey"; 
	private String tag; 
 
	public FREObject call(FREContext arg0, FREObject[] arg1) { 
		ExtensionContext ctx = (ExtensionContext) arg0; 
		tag = ctx.getIdentifier() + "." + KEY; 
		Log.i( tag, "Invoked " + KEY ); 
		
		try {			
			// Show it!
			Lootsie.showWebView(ctx.getActivity());
		
		} catch (Exception e) { 
			Log.d(tag, e.getMessage()); 
			e.printStackTrace(); 
		} 
		
		
		return null;		
	} 
}