package com.fgl.lootsieane;

import android.util.Log;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREExtension;
import com.lootsie.Lootsie;
 
public class Extension implements FREExtension { 
	private static final String EXT_NAME = "Extension"; 
	private ExtensionContext context; 
	private String tag = EXT_NAME + "ExtensionClass"; 	
	 
	public FREContext createContext(String arg0) { 
		Log.i(tag, "Creating context"); 
		if( context == null) context = new ExtensionContext(EXT_NAME); 
		return context; 
	} 
	 
	public void dispose() { 
		Log.i(tag, "Disposing extension"); 
		Lootsie.onDestroy(); 
	} 
	 
	public void initialize() { 
		Log.i(tag, "Initialize"); 
		// nothing to initialize for this example 
	} 
}