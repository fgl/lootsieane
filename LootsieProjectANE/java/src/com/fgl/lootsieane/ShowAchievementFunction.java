package com.fgl.lootsieane;

import android.util.Log;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;
import com.lootsie.Lootsie;
 
public class ShowAchievementFunction implements FREFunction { 
	public static final String KEY = "showAchievementFunctionKey"; 
	private String tag; 
 
	public FREObject call(FREContext arg0, FREObject[] arg1) { 
		ExtensionContext ctx = (ExtensionContext) arg0; 
		tag = ctx.getIdentifier() + "." + KEY; 
		Log.i( tag, "Invoked " + KEY ); 
		
		try {
			// What achieve do they want to show?
			String achievement = arg1[0].getAsString();
						
			// Show it!			
			Lootsie.AchievementReached(ctx.getActivity(), achievement);
		
		} catch (Exception e) { 
			Log.d(tag, e.getMessage()); 
			e.printStackTrace(); 
		} 
		
		
		return null;		
	} 
}