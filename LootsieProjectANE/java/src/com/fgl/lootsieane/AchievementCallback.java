package com.fgl.lootsieane;

import android.util.Log;

import com.lootsie.window.observers.IAchievementReached;

public class AchievementCallback implements IAchievementReached {
	private String tag = "AchievementCallback";
	
	/* (non-Javadoc)
	 * @see com.lootsie.window.observers.IAchievementReached#onLootsieBarClosed()
	 */
	@Override
	public void onLootsieBarClosed() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.lootsie.window.observers.IAchievementReached#onLootsieBarExpanded()
	 */
	@Override
	public void onLootsieBarExpanded() {
		// TODO Auto-generated method stub
		
	}

	/* (non-Javadoc)
	 * @see com.lootsie.window.observers.IAchievementReached#onLootsieFailed()
	 */
	@Override
	public void onLootsieFailed() {
		// TODO Auto-generated method stub
		Log.e(tag,"Achievement failed");
	}

	/* (non-Javadoc)
	 * @see com.lootsie.window.observers.IAchievementReached#onLootsieSuccess()
	 */
	@Override
	public void onLootsieSuccess() {
		// TODO Auto-generated method stub
		Log.d(tag,"Achievement Success");
	}
	
}
