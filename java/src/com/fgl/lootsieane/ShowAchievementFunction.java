package com.fgl.lootsieane;

import android.app.Activity;
import android.util.Log;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;
import com.lootsie.Lootsie;
import com.lootsie.callbacks.model.IAchievementReached;
 
public class ShowAchievementFunction implements FREFunction, IAchievementReached { 
	public static final String KEY = "showAchievementFunctionKey"; 
	private String tag;
	
	private static Activity activity; 
 
	public FREObject call(FREContext arg0, FREObject[] arg1) { 
		ExtensionContext ctx = (ExtensionContext) arg0; 
		tag = ctx.getIdentifier() + "." + KEY; 
		Log.i( tag, "Invoked " + KEY ); 
		
		try {
			// What achieve do they want to show?
			String achievement = arg1[0].getAsString();
			
			// Save a reference to the Activity
			activity = ctx.getActivity(); 
						
			// Show it!			
			Lootsie.AchievementReached(activity, achievement, Lootsie.DEFAULT_POSITION, this);
		
		} catch (Exception e) { 
			Log.d(tag, e.getMessage()); 
			e.printStackTrace(); 
		} 
		
		
		return null;		
	}

	/* (non-Javadoc)
	 * @see com.lootsie.callbacks.model.IAchievementReached#onLootsieBarClosed()
	 */
	@Override
	public void onLootsieBarClosed() {
		Log.d(tag, "onLootsieBarClosed"); 
	}

	/* (non-Javadoc)
	 * @see com.lootsie.callbacks.model.IAchievementReached#onLootsieBarExpanded()
	 */
	@Override
	public void onLootsieBarExpanded() {
		Log.d(tag, "onLootsieBarExpanded");
	}

	/* (non-Javadoc)
	 * @see com.lootsie.callbacks.model.IAchievementReached#onLootsieFailed()
	 */
	@Override
	public void onLootsieFailed() {
		Log.e(tag, "onLootsieFailed");
	}

	/* (non-Javadoc)
	 * @see com.lootsie.callbacks.model.IAchievementReached#onLootsieSuccess()
	 */
	@Override
	public void onLootsieSuccess() {
		Log.d(tag, "onLootsieSuccess");
	} 		
}