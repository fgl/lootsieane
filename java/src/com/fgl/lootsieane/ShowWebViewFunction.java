package com.fgl.lootsieane;

import android.app.Activity;
import android.util.Log;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;
import com.lootsie.Lootsie;
 
public class ShowWebViewFunction implements FREFunction { 
	public static final String KEY = "showWebViewFunctionKey"; 
	private String tag;
	
	private static Activity activity; 
 
	public FREObject call(FREContext arg0, FREObject[] arg1) { 
		ExtensionContext ctx = (ExtensionContext) arg0; 
		tag = ctx.getIdentifier() + "." + KEY; 
		Log.i( tag, "Invoked " + KEY ); 
		
		try {	
			// Save a reference to the Activity
			activity = ctx.getActivity(); 
						
			// Show it!
			Lootsie.showWebView(activity);
		
		} catch (Exception e) { 
			Log.d(tag, e.getMessage()); 
			e.printStackTrace(); 
		} 
		
		
		return null;		
	} 
}