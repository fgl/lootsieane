package com.fgl.lootsieane;

import android.app.Application;
import android.util.Log;

import com.adobe.fre.FREContext;
import com.adobe.fre.FREFunction;
import com.adobe.fre.FREObject;
import com.lootsie.Lootsie;
import com.lootsie.callbacks.model.IInitializationCallback;
 
public class InitFunction implements FREFunction, IInitializationCallback { 
	public static final String KEY = "initFunctionKey"; 
	private String tag;
	
	private static Application application; 
 
	public FREObject call(FREContext arg0, FREObject[] arg1) { 
		ExtensionContext ctx = (ExtensionContext) arg0; 
		tag = ctx.getIdentifier() + "." + KEY; 
		Log.d( tag, "Invoked " + KEY ); 
		 		 
		try { 
			// Read in the public key
			FREObject input = arg1[0]; 
			String publicKey = input.getAsString();
			
			// Set a reference to the application object
			application = arg0.getActivity().getApplication();
			
			Log.d(tag, "PublicKey received, attempting to init v1.5");
			Log.d(tag, publicKey);
						
			// Init Lootsie
			Lootsie.init(application, publicKey, this);									
			
		} catch (Exception e) { 
			Log.d(tag,"ERROR");
			Log.d(tag, e.getMessage()); 
			e.printStackTrace(); 
		} 	
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.lootsie.callbacks.model.IInitializationCallback#onInitFailure()
	 */
	@Override
	public void onInitFailure() {
		Log.e(tag,"Lootsie initialization Failure");
	}

	/* (non-Javadoc)
	 * @see com.lootsie.callbacks.model.IInitializationCallback#onInitSuccess()
	 */
	@Override
	public void onInitSuccess() {
		Log.d(tag,"Lootsie initialization Success");
	} 
	
	
}