adt="/Users/rogerbankus/Documents/developerFiles/air_15/bin/adt"
nativedir="/Users/rogerbankus/git/lootsieane/java"

echo "********************************************************************"
echo " - creating ANE package"

rm -rf Android-ARM/*
rm -f FGLLootsieExtension.ane library.swf
mkdir -p Android-ARM

cp external_jars/* Android-ARM

unzip ../as3/bin/LootsieSWC.swc library.swf
cp library.swf Android-ARM
cp "$nativedir"/LootsieANE.jar Android-ARM
cp -r "$nativedir"/res Android-ARM

"$adt" -package -target ane FGLLootsieExtension.ane extension.xml -swc ../as3/bin/LootsieSWC.swc -platform Android-ARM -platformoptions platform.xml -C Android-ARM . 

